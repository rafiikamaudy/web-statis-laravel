<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'film';
    protected $fillable = ['id_film', 'judul', 'ringkasan', 'tahun', 'poster', 'id_genre'];
    // kalo pake ini, nanti di tabelnya created_at sama updated_at ga ada
    public $timestamps = false;
}
