<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_cast' => 'required|max:255',
            'umur_cast' => 'required',
            'bio_cast' => 'required',
        ]);

        DB::table('cast')->insert(
            [
                'nama_cast' => $request['nama_cast'], 'umur_cast' => $request['umur_cast'],
                'bio_cast' => $request['bio_cast'],
                ]
        );

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        //compact buat lempar isi dari $cast
        return view('cast.index', compact('cast'));
    }

    public function show($id_cast)
    {
        $cast = DB::table('cast')->where('id_cast', $id_cast)->first();

        return view('cast.show', compact('cast'));

    }

    public function edit($id_cast)
    {
        $cast = DB::table('cast')->where('id_cast', $id_cast)->first();

        return view('cast.edit', compact('cast'));

    }

    public function update(Request $request, $id_cast){
        $request->validate([
            'nama_cast' => 'required|max:255',
            'umur_cast' => 'required',
            'bio_cast' => 'required',
        ]);

        // buat nyimpen update datanya
        DB::table('cast')
              ->where('id_cast', $id_cast)
              ->update(
                [
                      'nama_cast' => $request['nama_cast'],
                      'umur_cast' => $request['umur_cast'],
                      'bio_cast' => $request['bio_cast']
                ]
            );

            return redirect('/cast');
    }

    public function destroy($id_cast)
    {
        DB::table('cast')->where('id_cast', '=', $id_cast)->delete();
        return redirect('/cast');
    }
}


