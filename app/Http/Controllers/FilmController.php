<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\Film;
use File;
use DB;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::get();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::get();
        return view('film.tambah', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'ringkasan' => 'required|max:255',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            'id_genre' => 'required',
        ]);

        $imageName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('gambar'), $imageName);

        $film = new film;

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $imageName;
        $film->id_genre = $request->id_genre;

        $film->save();

        return redirect('/film/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_film)
    {
        $film = Film::where('id_film',$id_film)->first();
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_film)
    {
        $genre = Genre::get();
        $film = Film::where('id_film',$id_film)->first();
        return view('film.edit', compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_film)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'ringkasan' => 'required|max:255',
            'tahun' => 'required',
            'poster' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
            'id_genre' => 'required',
        ]);
    
        $film = DB::table('film')->where('id_film', $id_film);

        $film_qb = DB::table('film')->where('id_film', $id_film)->first();

        if ($request->has('poster')) {
            $path = "gambar/";
            File::delete($path . $film_qb->poster);
            $imageName = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('gambar'), $imageName);
            $film_data = [
                'judul'=> $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'poster' => $imageName,
                'id_genre' => $request->id_genre,
            ];

        } else {
            $film_data = [
                'judul'=> $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'id_genre' => $request->id_genre,
                ];
            }

            $film->update($film_data);

            return redirect('/film');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_film)
    {
        $film = DB::table('film')->where('id_film', $id_film);

        $film_qb = DB::table('film')->where('id_film', $id_film)->first();

        $film->delete();

        $path = "gambar/";

        File::delete($path . $film_qb->poster);
        
        return redirect('/film');

    }


}
