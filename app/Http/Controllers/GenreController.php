<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255',
        ]);

        DB::table('genre')->insert(
            [
                'nama' => $request['nama']
            ]
        );

        return redirect('/genre');
    }

    public function index()
    {
        $genre = DB::table('genre')->get();

        return view('genre.index', compact('genre'));
    }

    public function edit($id_genre)
    {
        $genre = DB::table('genre')->where('id_genre', $id_genre)->first();

        return view('genre.edit', compact('genre'));
    }

    public function update(Request $request, $id_genre)
    {
        $request->validate([
            'nama' => 'required|max:255',
        ]);
        
        DB::table('genre')
            ->where('id_genre', $id_genre)
            ->update(
                  [
                      'nama' => $request['nama']
                      ]
            );

            return redirect('/genre');
    }

    public function destroy($id_genre)
    {
        DB::table('genre')->where('id_genre', '=', $id_genre)->delete();
        return redirect('/genre');
    }
}
