<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peran', function (Blueprint $table) {
            $table->bigIncrements('id_peran');
            $table->unsignedBigInteger('id_film');
            $table->foreign('id_film')->references('id_film')->on('film');
            $table->unsignedBigInteger('id_cast');
            $table->foreign('id_cast')->references('id_cast')->on('cast');
            $table->string('nama_peran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peran');
    }
}
