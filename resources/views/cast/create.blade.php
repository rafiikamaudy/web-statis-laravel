@extends('layout.master')

@section('judul')
    Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" class="form-control" name="nama_cast" placeholder="Masukkan Nama Cast">
        @error('nama_cast')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Umur Cast</label>
        <input type="number" class="form-control" name="umur_cast" placeholder="Masukkan Umur Cast">
        @error('umur_cast')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Bio Cast</label>
        <textarea name="bio_cast" id="" class="form-control" cols="30" rows="10"></textarea>
        @error('bio_cast')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form> 
@endsection
