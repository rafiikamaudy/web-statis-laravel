@extends('layout.master')

@section('judul')
    Edit Cast {{$cast->nama_cast}}
@endsection

@section('content')
<form action="/cast/{{$cast->id_cast}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$cast->nama_cast}}" name="nama_cast" placeholder="Masukkan Nama Cast">
        @error('nama_cast')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Umur</label>
        <input type="number" class="form-control" name="umur_cast" placeholder="Masukkan Umur Cast">
        @error('umur_cast')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio_cast" id="" class="form-control" cols="30" rows="10">{{$cast->bio_cast}}</textarea>
        @error('bio_cast')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
</form> 
@endsection
