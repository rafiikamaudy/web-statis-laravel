@extends('layout.master')

@section('judul')
    List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary mb-2">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama_cast}}</td>
                        <td>{{$value->umur_cast}}</td>
                        <td>{{$value->bio_cast}}</td>
                        <td>
                            <a href="/cast/{{$value->id_cast}}" class="btn btn-info mb-2">Show</a>
                            <a href="/cast/{{$value->id_cast}}/edit" class="btn btn-primary mb-1">Edit</a>
                            <form action="/cast/{{$value->id_cast}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection