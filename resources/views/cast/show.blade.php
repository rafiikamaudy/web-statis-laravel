@extends('layout.master')

@section('judul')
    Cast {{$cast->nama_cast}}
@endsection

@section('content')
<h1>{{$cast->nama_cast}}</h1>
<p>{{$cast->umur_cast}}</p>
<p>{{$cast->bio_cast}}</p>
@endsection