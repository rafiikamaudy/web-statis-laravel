@extends('layout.master')

@section('judul')
    Edit Film {{$film->judul}}
@endsection

@section('content')
<form action="/film/{{$film->id_film}}" method="POST" enctype="multipart/form-data">
    @method('put')
    @csrf
    <div class="form-group">
        <label>Judul Film</label>
        <input type="text" class="form-control" name="judul" value="{{$film->judul}}" placeholder="Masukkan Judul Film">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Ringkasan Film</label>
        <textarea name="ringkasan" id="" class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Tahun Film</label>
        <input type="number" class="form-control" name="tahun" value="{{$film->tahun}}" placeholder="Masukkan Tahun Film">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Poster Film</label>
        <input type="file" class="form-control" name="poster" >
        @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Genre</label>
        <select name="id_genre" id="" class="form-control">
            <option value="">-- Silahkan Pilih Genre --</option>
            @foreach ($genre as $item)
                @if ($item->id_genre === $film->id_genre)
 
                    <option value="{{$item->id_genre}}" selected>{{$item->nama}}</option>

                 @else

                    <option value="{{$item->id_genre}}">{{$item->nama}}</option>
                
               
                @endif
            @endforeach
        </select>
        @error('id_genre')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
</form> 
@endsection