@extends('layout.master')

@section('judul')
    List Film
@endsection

@section('content')

<a href="/film/create" class="btn btn-primary btn-sm my-2">Tambah</a>

<div class="row">
    @forelse ($film as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('gambar/'.$item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5>{{$item->judul}}</h5>
              <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>
              <form action="/film/{{$item->id_film}}" method="POST">
            @method('DELETE')
            @csrf
            <a href="/film/{{$item->id_film}}" class="btn btn-info btn-sm">Read More</a>
            <a href="/film/{{$item->id_film}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                 </form>
            </div>
          </div>
    </div>
    @empty
        <h5>Tidak ada film</h5>
    @endforelse
</div>
@endsection

