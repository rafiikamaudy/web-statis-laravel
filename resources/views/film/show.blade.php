@extends('layout.master')

@section('judul')
    Detail Film {{$film->judul}}
@endsection

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <img src="{{asset('gambar/'.$film->poster)}}" width="200px" alt="...">
            <div class="card-body">
              <h5>{{$film->judul}}</h5>
              <p class="card-text">{{$film->ringkasan}}</p>
            </div>
          </div>
    </div>


</div>

<a href="/film" class="btn btn-secondary btn-sm my-2">Kembali</a>

@endsection

