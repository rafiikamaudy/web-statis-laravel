@extends('layout.master')

@section('judul')
    Tambah Film
@endsection

@section('content')
<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Judul Film</label>
        <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul Film">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Ringkasan Film</label>
        <textarea name="ringkasan" id="" class="form-control" cols="30" rows="10"></textarea>
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Tahun Film</label>
        <input type="number" class="form-control" name="tahun" placeholder="Masukkan Tahun Film">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Poster Film</label>
        <input type="file" class="form-control" name="poster">
        @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Genre</label>
        <select name="id_genre" id="" class="form-control">
            <option value="">-- Silahkan Pilih Genre --</option>
            @foreach ($genre as $item)
                <option value="{{$item->id_genre}}">{{$item->nama}}</option>
            @endforeach
        </select>
        @error('id_genre')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form> 
@endsection