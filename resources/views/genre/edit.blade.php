@extends('layout.master')

@section('judul')
    Edit Genre {{$genre->nama}}
@endsection

@section('content')
<form action="/genre/{{$genre->id_genre}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama Genre</label>
        <input type="text" class="form-control" value="{{$genre->nama}}" name="nama" placeholder="Masukkan Nama Genre">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
</form> 
@endsection