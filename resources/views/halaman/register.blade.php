<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="firstname"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="lastname"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="nationality">
            <option value="indonesian"> Indonesian</option> <br><br>
            <option value="singaporean">Singaporean</option> <br><br>
            <option value="malaysian">Malaysian</option> <br><br>
            <option value="australian">Australian</option> 
        </select> <br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>