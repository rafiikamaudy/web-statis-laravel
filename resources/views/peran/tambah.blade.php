@extends('layout.master')

@section('judul')
    Tambah Peran
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf

    <div class="form-group">
        <label>Pilih Film</label>
        <select name="id_film" id="" class="form-control">
            <option value="">--Silahkan pilih film--</option>
            @foreach ($film as $item)
            <option value="{{$item->id_film}}">{{$item->judul}}</option>
        </select>
        @error('id_film')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Pilih Cast</label>
        <select name="id_cast" id="" class="form-control">
            <option value="">--Silahkan pilih cast--</option>
            @foreach ($cast as $item1)
            <option value="{{$item1->id_cast}}">{{$item1->nama_cast}}</option>
        </select>
        @error('id_cast')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Nama Peran</label>
        <input type="text" class="form-control" name="nama_peran" placeholder="Masukkan Nama Peran">
        @error('nama_peran')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form> 
@endsection
