<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::get('/table', function(){
    return view('table.table');
});

// CRUD Cast

// untuk mengarah ke form cast
Route::get('/cast/create', 'CastController@create');

// untuk insert data ke database
Route::post('/cast', 'CastController@store');

// mengambil semua data pada tabel cast
Route::get('/cast', 'CastController@index');

// menampilkan detail data
Route::get('/cast/{id_cast}', 'CastController@show');

//mengarah ke form edit cast
Route::get('/cast/{id_cast}/edit', 'CastController@edit');

// fungsi update berdasarkan id
Route::put('/cast/{id_cast}', 'CastController@update');

// fungsi delete berdasarkan id
Route::delete('/cast/{id_cast}', 'CastController@destroy');

// CRUD Genre query builder

// untuk mengarah ke form genre
Route::get('/genre/create', 'GenreController@create');

// untuk insert data ke database
Route::post('/genre', 'GenreController@store');

// mengambil semua data pada tabel genre
Route::get('/genre', 'GenreController@index');

// mengarah ke form edit genre
Route::get('/genre/{id_genre}/edit', 'GenreController@edit');

// fungsi update berdasarkan id
Route::put('/genre/{id_genre}', 'GenreController@update');

// fungsi delete berdasarkan id
Route::delete('/genre/{id_genre}', 'GenreController@destroy');

// CRUD ORM (ga satu-satu)
Route::resource('film', 'FilmController');

// CRUD update query builder
Route::get('/film/{id_film}/edit', 'FilmController@edit');